/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "dwt_delay.h"
#include "lcd_i2cModule.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

//Le damos nombre al pin al que est� conectada cada fila y columna:
//#define C1_PORT GPIOA
#define C1_PIN GPIO_PIN_1

//#define C2_PORT GPIO_A
#define C2_PIN GPIO_PIN_2

//#define C3_PORT GPIO_A
#define C3_PIN GPIO_PIN_3

//#define R1_PORT GPIO_A
#define R1_PIN GPIO_PIN_4

//#define R2_PORT GPIO_A
#define R2_PIN GPIO_PIN_5

//#define R3_PORT GPIO_A
#define R3_PIN GPIO_PIN_6

//#define R4_PORT GPIO_A
#define R4_PIN GPIO_PIN_7

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

I2C_HandleTypeDef hi2c1;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;

/* USER CODE BEGIN PV */
volatile float Inst_recepcion;
volatile int vuelta = 0;
volatile int marca = 0;
int distancia;
int tiempo;
//int flag_PAD = 1;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
static void MX_TIM1_Init(void);
static void MX_ADC1_Init(void);
static void MX_TIM2_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if(GPIO_Pin == GPIO_PIN_0)
		marca = 1;
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){
	if(htim -> Instance == TIM2 )
		flag_PAD = 0;
}

//Medimos la distancia con el sensor de ultrasonidos:
float medir_distancia(){
		float Inst_envio,Distancia_medida;
		//Enviamos el ultrasonido:
		HAL_GPIO_WritePin(GPIOB,GPIO_PIN_1,0);
		DWT_Delay(4);//Esperamos 4 microsegundos
		HAL_GPIO_WritePin(GPIOB,GPIO_PIN_1,1);
		DWT_Delay(10);//Esperamos 10 microsegundos
		HAL_GPIO_WritePin(GPIOB,GPIO_PIN_1,0);
		//Almacenamos el instante de env�o del ultrasonido (en microsegundos):
		Inst_envio = 1000*HAL_GetTick();
		while(vuelta == 0) {}
		//Tenemos en cuenta la f�rmula "distancia=velocidad*tiempo" y que la velocidad 
		//del sonido es aproximadamente 0,0343 cm/us, obtenemos la distancia:
		Distancia_medida = 0.0343*(Inst_recepcion-Inst_envio)/2;
		vuelta = 0;
		return Distancia_medida;
	}

//Esta funci�n nos devolver� un 1 o un 0 seg�n est� o no la tecla pulsada, respectivamente:
int lee_letra_pulsada(uint16_t pin) {
	int delay_rebotes = 100;
		if(!(HAL_GPIO_ReadPin(GPIOA, pin))){
			HAL_Delay(delay_rebotes);  //Aplicamos un retardo para evitar el ruido al pulsar.
			if(!(HAL_GPIO_ReadPin(GPIOA, pin))){ 
				while(!(HAL_GPIO_ReadPin(GPIOA, pin)));
				HAL_Delay(delay_rebotes);  //Volvemos a aplicar el retardo para eliminar el ruido producido al dejar de pulsar.
				return 1;  //Tecla pulsada
			}
		}
		return 0;  //Tecla no pulsada
}

//Hacemos un barrido por filas en el teclado matricial:
char obten_tecla (void){
	//flag_PAD = 1;
	//HAL_TIM_Base_Start_IT(&htim2);
	while (1) {
		//Comprobamos si hay alguna tecla pulsada en la PRIMERA FILA (la ponemos a 0 y el resto a 1):
		HAL_GPIO_WritePin(GPIOA, R1_PIN, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOA, R2_PIN, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOA, R3_PIN, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOA, R4_PIN, GPIO_PIN_SET);
		//Leemos cada una de las tres columnas una a una:
		if (lee_letra_pulsada(C1_PIN)) return '1';
		if (lee_letra_pulsada(C2_PIN)) return '2';
		if (lee_letra_pulsada(C3_PIN)) return '3';
		
		//Comprobamos si hay alguna tecla pulsada en la SEGUNDA FILA (la ponemos a 0 y el resto a 1):
		HAL_GPIO_WritePin(GPIOA, R1_PIN, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOA, R2_PIN, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOA, R3_PIN, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOA, R4_PIN, GPIO_PIN_SET);
		//Leemos cada una de las tres columnas una a una:
		if (lee_letra_pulsada(C1_PIN)) return '4';
		if (lee_letra_pulsada(C2_PIN)) return '5';
		if (lee_letra_pulsada(C3_PIN)) return '6';
		
		//Comprobamos si hay alguna tecla pulsada en la TERCERA FILA (la ponemos a 0 y el resto a 1):
		HAL_GPIO_WritePin(GPIOA, R1_PIN, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOA, R2_PIN, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOA, R3_PIN, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(GPIOA, R4_PIN, GPIO_PIN_SET);
		//Leemos cada una de las tres columnas una a una:
		if (lee_letra_pulsada(C1_PIN)) return '7';
		if (lee_letra_pulsada(C2_PIN)) return '8';
		if (lee_letra_pulsada(C3_PIN)) return '9';
		
		//Comprobamos si hay alguna tecla pulsada en la CUARTA FILA (la ponemos a 0 y el resto a 1):
		HAL_GPIO_WritePin(GPIOA, R1_PIN, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOA, R2_PIN, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOA, R3_PIN, GPIO_PIN_SET);
		HAL_GPIO_WritePin(GPIOA, R4_PIN, GPIO_PIN_RESET);
		//Leemos cada una de las tres columnas una a una:
		if (lee_letra_pulsada(C1_PIN)) return '*';
		if (lee_letra_pulsada(C2_PIN)) return '0';
		if (lee_letra_pulsada(C3_PIN)) return '#';
	}
	return 'X';
}


uint8_t tecla;


char key[4] = "1245";  //Contrasena establecida
char key_digits[4];  //Almacena los d�gitos introducidos

int introducir_clave (){
	LCD_Clear();
	LCD_SetCursor(1, 1);
	LCD_Send_String("CLAVE: ", STR_NOSLIDE);

	int i = 0;
	//Leemos los 4 digitos
	while (i < 4) {
		//Lee tecla
		tecla = obten_tecla();
		if (tecla == 'X')
			return 2;
		i++;

		//Guarda tecla
		key_digits[i - 1] = tecla;

		//Pinta tecla
		LCD_SetCursor(1, 7 + i);
		LCD_Write_Data(tecla);
	}

	//Comprueba codigo
	int codigo_es_correcto = 1;
	for (int j = 0; j < 4; j++){
		if(key[j] != key_digits[j]){
			codigo_es_correcto = 0;
		}
	}

	//Muestra comprobacion del codigo
	LCD_SetCursor(2, 1);
	if (codigo_es_correcto) {
		LCD_Send_String("CORRECTO", STR_NOSLIDE);
		return 1;
	} else {
		LCD_Send_String("INCORRECTO", STR_NOSLIDE);
		return 0;
	}
	HAL_Delay(2000);
}

int valorLDR;

int lectura_analogica (){
	HAL_ADC_Start (&hadc1); //Iniciamos el convertidor
	if(HAL_ADC_PollForConversion(&hadc1, HAL_MAX_DELAY)==HAL_OK) //Solicita el valor del convertidor
	{
		valorLDR = HAL_ADC_GetValue(&hadc1);  //recibe el valor del convertidor
	}
	HAL_ADC_Stop(&hadc1);
	
	if(valorLDR<0x20)
		return 1;
	else
		return 0;
}

int hcsr04_read(void){
int local_time=0;
		HAL_GPIO_WritePin(GPIOB,Pin_Trig_Pin,GPIO_PIN_SET);// poner trig en 1
		DWT_Delay(10);
		HAL_GPIO_WritePin(GPIOB,Pin_Trig_Pin,GPIO_PIN_RESET);// resetear trig
	
	//Esperar que echo reciba 1
		while(HAL_GPIO_ReadPin(GPIOB,Pin_Echo_Pin))
		{
			local_time++;
			
		}
		return local_time;
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_TIM1_Init();
  MX_ADC1_Init();
  MX_TIM2_Init();
  /* USER CODE BEGIN 2 */

 //Inicializamos el "delay":
	DWT_Init();
	
	//Verificamos el dispositivo esclavo (la pantalla LCD):
	LCD_i2cDeviceCheck();
	//Iniciamos su configuraci�n:
	LCD_Init();
	//Encendemos la luz del fondo:
	LCD_BackLight(LCD_BL_ON);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

	//Habr� 3 estados posibles: de espera (S0), de alerta (S1) y de encendido (S2):
	enum ESTADOS{S0,S1,S2,S3};
	int state = S0;
	int t_0;
	
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
		tiempo=hcsr04_read();
		distancia=tiempo*0.034/2;
		
		//LDR
		if (lectura_analogica())
			LCD_BackLight(LCD_BL_ON);
		else
			LCD_BackLight(LCD_BL_OFF);
		
		
		switch (state)
		{
			case S0:	//ESTADO DESACTIVADO			
				if (marca) {
					if(introducir_clave()==1){
						state = S1;
						HAL_Delay(2000);
						LCD_Clear();
					}
				}
				else 
				{
					LCD_Clear();
					LCD_SetCursor(1,5);
					LCD_Send_String("ALARMA", STR_NOSLIDE);
					LCD_SetCursor(2,3);
					LCD_Send_String("DESACTIVADA", STR_NOSLIDE);
					state = S0;
					HAL_Delay(200);
				}
			break;
			case S1:	//ESTADO ACTIVO
				//Aparece el mensaje "ALERTA" en la pantalla:
				LCD_SetCursor(1,6);
				LCD_Send_String("ACTIVO", STR_NOSLIDE);
				//distancia = medir_distancia();
				if(distancia<50)//marca)
					state = S2;
				else
					state = S1;
			break;
			case S2:	//ESTADO PREALERTA
				t_0 = HAL_GetTick();
				while((HAL_GetTick()-t_0) < 10000){
					LCD_Clear();
					HAL_Delay(1000);
					LCD_SetCursor(1,6);
					LCD_Send_String("ALERTA", STR_NOSLIDE);
					//HAL_GPIO_TogglePin (GPIOE,GPIO_PIN_9);
					HAL_Delay(1000);
				}
				state = S3;
				//Aparece el mensaje "INTRODUZCA CONTRASENA" en la pantalla
				
				//Si se introduce bien deja de sonar y se regresa al estado de espera. Sino, continuar� sonando
         //y querr� decir que es un ladr�n la persona detectada
			break;
				
			case S3:	//ESTADO ALERTA
				if(marca){
					if(introducir_clave()==1){
					LCD_Clear();
					LCD_SetCursor(1,3);
					LCD_Send_String("DESACTIVADO", STR_NOSLIDE);
					HAL_Delay(1000);
					state = S0;
					}
				}
				else
					state = S3;				
			break;
  }
		
	
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 72;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC1_Init(void)
{

  /* USER CODE BEGIN ADC1_Init 0 */

  /* USER CODE END ADC1_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC1_Init 1 */

  /* USER CODE END ADC1_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
  */
  hadc1.Instance = ADC1;
  hadc1.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV2;
  hadc1.Init.Resolution = ADC_RESOLUTION_8B;
  hadc1.Init.ScanConvMode = DISABLE;
  hadc1.Init.ContinuousConvMode = DISABLE;
  hadc1.Init.DiscontinuousConvMode = DISABLE;
  hadc1.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1.Init.NbrOfConversion = 1;
  hadc1.Init.DMAContinuousRequests = DISABLE;
  hadc1.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_10;
  sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC1_Init 2 */

  /* USER CODE END ADC1_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 0;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 0;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 72;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 10000;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_OC_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_TIMING;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_OC_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, Row_1_Pin|Row_2_Pin|Row_3_Pin|Row_4_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(Pin_Trig_GPIO_Port, Pin_Trig_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : Pulsador_Usuario_Pin */
  GPIO_InitStruct.Pin = Pulsador_Usuario_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(Pulsador_Usuario_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : Column_1_Pin Column_2_Pin Column_3_Pin */
  GPIO_InitStruct.Pin = Column_1_Pin|Column_2_Pin|Column_3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : Row_1_Pin Row_2_Pin Row_3_Pin Row_4_Pin */
  GPIO_InitStruct.Pin = Row_1_Pin|Row_2_Pin|Row_3_Pin|Row_4_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pin : Pin_Echo_Pin */
  GPIO_InitStruct.Pin = Pin_Echo_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(Pin_Echo_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : Pin_Trig_Pin */
  GPIO_InitStruct.Pin = Pin_Trig_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(Pin_Trig_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
