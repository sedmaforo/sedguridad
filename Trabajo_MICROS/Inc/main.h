/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define Pulsador_Usuario_Pin GPIO_PIN_0
#define Pulsador_Usuario_GPIO_Port GPIOA
#define Pulsador_Usuario_EXTI_IRQn EXTI0_IRQn
#define Column_1_Pin GPIO_PIN_1
#define Column_1_GPIO_Port GPIOA
#define Column_2_Pin GPIO_PIN_2
#define Column_2_GPIO_Port GPIOA
#define Column_3_Pin GPIO_PIN_3
#define Column_3_GPIO_Port GPIOA
#define Row_1_Pin GPIO_PIN_4
#define Row_1_GPIO_Port GPIOA
#define Row_2_Pin GPIO_PIN_5
#define Row_2_GPIO_Port GPIOA
#define Row_3_Pin GPIO_PIN_6
#define Row_3_GPIO_Port GPIOA
#define Row_4_Pin GPIO_PIN_7
#define Row_4_GPIO_Port GPIOA
#define Pin_Echo_Pin GPIO_PIN_1
#define Pin_Echo_GPIO_Port GPIOB
#define Pin_Trig_Pin GPIO_PIN_2
#define Pin_Trig_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
